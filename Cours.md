# Documentation 
   
## Premier Cours   


### Markdown
Le Markdown n'est pas un langage de programmation. 
En effet, c'est un langage de balisage. Il utilise 
une syntaxe simple pour être saisie par 
les editeurs de textes les plus basique...  
Il permet de faciliter la prise de note et rendre plus 
rapide, efficace et professionnelle l'écriture de texte simple.

Certain éditeurs WISIWIG(What I see is what I get)
comme Word permettent de visualiser le resultat des balises markdown.

![Assets/img/WYSIWYG.jpg](Assets/img/WYSIWYG.jpg)

### Git

Git est un protocole qui permet de stocker des données.   
Github et gitlab sont des sites internet qui utilisent ce protocole.
   
 ![.git](Assets/img/git.jpg)
 
### Backtick
Les backticks `(*3) en markdown permettent de placer du code au millieu
d'un texte.
 
   
## Second Cours   

### Fichier html
Un navigateur Internet est un "parser", qui parcours les fichiers html.
Il faut d'abord s'occuper de la répartition du contenu avant 
l'esthétique du site afin d'optimiser la recherche par 
navigateur web. La première chose a déterminer avant de créer un
site est le nombre de pages qu'ils y'aura.

En général =  
- Homepage/Landing page
- About
- F.A.Q.

(Il faut penser a ajouter les assets sur git)

``` html
<!-- Les balises section, header et footer servent
a mieux reférencer le site sur les navigateurs -->
<section>
    <header> 
        <!-- Sous-titre -->
    </header>
    
    <!-- Contenu de la section -->

</section>
<!-- 2ème section, Le site est en deux parties -->
<section>
    <header> 
        <!-- Sous-titre -->
    </header>
    <footer>
        <!-- Fin de section -->
    </footer>
</section>
```

### Bootstrap

Alt + Entrée sur une librairie pour la télécharger sur Webstorm

Pour utiliser bootstrap, il suffit de coller un lien pour utiliser
le code de cette framework.

Ensuite, il suffit d'utiliser les components au lieu de créer des
classe en css ce qui en theorie, nous permet de ne pas avoir a
utiliser css.

Le bootstrap fonctionne avec une grille.
Il faut alors diviser le site en rangées et en colonne.
Il peut pas y'avoir plus de 12 colonnes par rangées.
Il faut mettre obligatoirement au moins une colonne a chaque rangées.
La div container est pratique pour faciliter le "responsive" du site.

Pour mettre un lien, il faut écrire: ```<a href="lien vers la page">lien visible</> ```

## Troisième Cours   

### .gitignore

Les fichiers comme .vcs(qui commencent par un point) sont des 
fichiers cachées qui servent par exemple a la configuration.

Pour empecher certains fichiers d'être envoyés, il suffit de 
creer un fichier .gitignore

Il ne faut pas supprimer le dossier .git

Il faut commit & push a quasiment chaque modification de son projet
afin de pouvoir revenir a chaque commit sur gitlab.

### Personalisation de Bootstrap

Pour personaliser bootstrap, il y'a plusieurs méthodes. 
La plus simple est de surcharger le css de bootstrap.
Il faut créer une nouvelle feuille de styles .css
Il suffit ensuite de lier ce nouveau lien dans la tête 
du lien htlm avant le lien bootstrap pour que les modifications 
effectués dans cette feuille de style s'effectuent par dessus de
celle de bootstrap.

### Import

Le mieux, avec cette méthode, est de créer une feuille css pour 
chaque modifications et les importer sur une feuille css, les 
regroupant. Import marche en cascade, donc il est possible d'importer 
a la fois bootstrapCustom.css et otherStyles.css, si ces deux feuilles 
de styles sont importés dans allStyles.css et que seulement celui-ci soit
importé dans le fichier htlm.

   
## Quatrième Cours 

### CDN 

Dans chaque site internet, il y'a une partie statique et une partie
dynamique.
ex:

|Statique|Dynamique|
|---|---|
|Html, css, Javascript|Base de données|

Un site internet est une architecture client/serveur. Les clients
vont taper une URL qui sert d'adresse pour acceder au site. L'URL
demande les informations qui permettent de construire le site avec 
un fichier html, css, js, ect... La page html ne peut pas être stockées
sur une seule machine car si des milliers d'ordinateurs tente de se 
connecter a cette page, le serveur ne tiendrai pas.
C'est pour cela que ces fichiers sont stockées sur un réseau de machine.  
Ces réseaux s'appelent des CDN. A travers le monde entier, n'importe 
qui peut acceder a un site en se connectant sur le CDN le plus proche 
de soi.
Le CDN va allouer des serveurs pour host les sites internet.
Pour lancer le deploiment, il faut en général un fichier html a la 
racine et créer un fichier public (et tout mettre dedans).
Dans le .gitignore, il faut rajouter .firebase (mais pas firebase.rc 
et firebase.json)

CDN = Content Delivery Network

### Gitflow

Le commit régulier permet de garder son code propre, et d'être pris 
au serieux par ses collègues.   
Le gitflow consiste  séparer le travail en plusieurs taches que chacun
accomplit sur une branch du projet. Ensuite, les codeurs envoient
leurs travail sur la master via merge request.

|  |Master|  |
|---|---|---|
|Branch A|Master|Branch B|
|Commit| |Commit|
|Commit|  |Commit|
|Commit|  |Commit|
|Merge Request to master|  |Commit|
|  |Merged with A  |Commit|
|  |  |Try to Merge Request|
|  |  See conflict and deny Merge Request with B|  |
|  | Send merged Master to A & B |  |
|Try to solve the conflicts|  |Try to solve the conflicts|
|Merge Request|   |Merge Request|
|  |Merged with A & B  |  |

## Cinquième cours

### JavaScript Array

Alt + Ctrl + l = Mettre la page a la norme

Un Array(tableau)est un objet qui permet de stocker d'autre objets.


``` js
(element     0  1  2  3  4    5      6                   7                  8)
const arrayName = [1, 2, 3, 4, 5, false, 4 < 5, {firstName: 'seb', age : 25}, [2, 3, 4]];

console.log(arrayName.length);

console.log (arrayName.2);    

console.log (arrayName[2] + arrayName[3]);    

console.log (arrayName.length - 1);    
```

#### length

arrayName.length permet d'avoir le nombre d'éléments dans un array.  
arrayName.2 permet d'avoir le deuxième élément dans un array.  
arrayName[0] + [3];    
 // 1 + 4 = 5
rays]   


En général, le tableau est constitoé du même type de données
ex:   
[str, str, str, str]   
[numbers, numbers, numbers]   
[objects, objects]   
[arrays, arrays, arrays]

#### forEach

``` js
const arrayName = [
    {firstName: 'asebu', age : 25}
    {firstName: 'jiseb', age : 92}
    {firstName: 'ilsebd', age : 27}
    {firstName: 'posebo', age : 38}
    {firstName: 'kiseby', age : 22}
    {firstName: 'fesebbeb', age : 47}
    {firstName: 'rysebs', age : 15}
];

arrayName.forEach(element => console.log(element))
```

console.log est appliqué pour chaque élément du tableau.

``` js
arrayName.forEach(element => console.log(element.age))
arrayName.forEach(element => console.log(timesTwo(element.age))

function timesTwo(x) {
    return x * 2
}
```

console.log est appliqué et affiche uniquement l'âge chaque 
élément du tableau.

Avec la fonction, console.log est appliqué et affiche 
l'âge multipliée par deux chaque élément du tableau.
  
#### Map
``` js
const ageArray = arrayName.map(element => element.age * 10)

console.log(ageArray)
```

.map applique une fonction sur chaque element et les transforme.


``` js


const ageArray = arrayName.map((element, index) => {
    const temp ={};
    temp.id = index;
    temp.promo = 'promo1';
    temp.firstName = element.firstName;
    temp.age = element.age;
    return temp;
});

console.log(ageArray)
```

Pour ne pas modifier ageArray, on peut créer un nouvel objet(ici, temp), pour

#### Sort

``` js
const orderedArray = ageArray ((a, b) => b.age - a.age)

console.log(orderedArray);

```
    
    
    
    
## Documentation  .md 
 
 Ajouter un # au début d'une phrase équivaut a une balise h1. Ajouter 7# equivaut
 a une balise h7
 
## h1 Titre    
### h2 Sous-Titre   
#### h3  
##### h4  
###### h5
  

### **  **Gras**  **  
### ~~ ~~Barré~~ ~~  

>(>)blockchain
>>(>>)nested blockchain
>>>(>>>)doubled nested blockchain

###__ _italique_ __

`+`   
`-`   
`*`  
### Liste
- A
+ B
* C  

### Liste en ordre numérotés
1. 
2.
3.

### Coder sur la même `ligne`

### Coder avec une indentation

    //commentaire
    ligne 1
    ligne 2
    ligne 3
 
 ```
Exemple
```

``` js
let s = true
console.log(s)
```

### Tableau

(Tableau |-----------|)

| Objet | Valeur |
|------|-----------|
|Canne à péche | 70$|
|Bouteille d'eau| 1$|
|Appartement|265000$|
|Voiture|2750$|

(Tableau aligné a droite
|--------:|)

| Objet | Valeur |
| ------:|-----------:|
|Canne à péche | 70$|
|Bouteille d'eau| 1$|
|Appartement|265000$|
|Voiture|2750$|

### Lien internet

Il suffit de rentrer [lien] (http://dfsdf.io/ "lien!") (_sans espace entre [] et ()_)

[lien](http://dfsdf.io/ "lien!")

https://dfsdf.io/

### Images

Pour afficher une image, il suffit de rentrer le lien de cette image depuis
internet ou de rentrer son adresse dans le projet.

![Image1] (https://dfsdf.io/image1.png)  
![Image2] (https://dfsdf.io/image2.png)
![Portrait] (Assets/Portrait.jpg)

(Sans espace entre [] et ().)

## Documentation Bootstrap

## Documentation CSS 

``` css
h1 {
    color: black;
    text-align: center
}

#para1



/* C'est un commentaire
sur plusieurs lignes */





```