# Ait-Bihi Safi

![Portrait](../Assets/img/Portrait.jpg)

## Expériences professionelles

### Primfleur
### 28, Rue Ampère; 75017 Paris
#### (De décembre 2013 à janvier 2014)
+ Mémorisation du prix des articles et j'ai vendu des sapins.
- Passage du  balai à l'extérieur et à l'intérieur du magasin.
-﻿ Livraison des sapins en les portant et en les installant dans des appartements et des bureaux.</p>
- Assistance d'un ﬂeuriste professionnel à s'occuper d'un jardin.
- ﻿Dés-épinage de ﬂeurs durant une période prolongé.
﻿- Installation de décorations de Noël.
## Compétences
### Vente
### Travail d'équipe     
### Communication
### Autonomie  


## Informatique
JavaScript (en cours d'acquisition)  
Html (en cours d'acquisition)  
Css (en cours d'acquisition)  
Python (en cours d'acquisition)    


### Email
safiaitbihi@gmail.com  
### Adresse
29, rue du haut de la noue, 92390 Villeneuve-La-Garenne  
### Âge
20 ans  
### Numéro de téléphone
07 67 98 86 30  
## Langues
Anglais  
Espagnol  
Latin
                           
## Atouts
Vitesse d'adaptation
Motivation
Sang-Froid
                        
## Centres d'intérêt
Programmation  
Basket-Ball  
Kung-Fu  
Arts